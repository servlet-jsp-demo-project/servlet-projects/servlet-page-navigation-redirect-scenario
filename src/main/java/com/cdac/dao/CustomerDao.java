package com.cdac.dao;

import com.cdac.pojo.Customer;

public interface CustomerDao {

	Customer authenticateCustomer(String email, String password) throws Exception;

	void cleanUp() throws Exception;
	
}
